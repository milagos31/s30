//Create Express Server
const express = require("express");
const app = express();
const port = 3001;

//mongoose
const mongoose = require("mongoose");

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//GET (trial to see if server is working)
app.get("/", (req, res) => {
    console.log('Someone accessed url: "' + req.url + '"');
    res.send("Successfully created a GET request");
});

//RUN the server
app.listen(port, () => console.log("Server is running in port: " + port));

/*
1. MongoDB connect
2. mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
3. Change myFirstDatabase to s30
4. mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/s30?retryWrites=true&w=majority
*/

//CONNECT to MongoDB
/*
ADD object to allow connection:
   {
      useNewUrlParser: true,
      useUnifiedTopology: true
   }
*/
const uri = `mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/s30?retryWrites=true&w=majority`;
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

//NOTIFICATION for connection
let db = mongoose.connection;

//ERROR handling on MongoDB
db.on("error", console.error.bind(console, "connection error"));

//SUCCESS handling on MongoDB
db.once("open", () => console.log("You're now connected to MongoDB Atlas!"));

//SCHEMA creation
const taskSchema = new mongoose.Schema({
    name: String,
    pending: { type: Boolean, default: true },
    dateCreated: { type: Date, default: new Date() },
    dateFinished: { type: Date, default: null },
});

//MODEL making for mongoose (Use proper case for the MODEL)
const Task = mongoose.model("Task", taskSchema);

//POST
app.post("/tasks", (req, res) => {
    console.log(`Trying to POST something in ${req.url.replace("/", "")} collection`);
    //DATA VALIDATION (if tasks already pending return invalid input)
    Task.findOne({ name: req.body.name }, (err, result) => {
        if (result != null && result.name == req.body.name && result.pending) {
            console.log(`task "${req.body.name}" is still pending since ${result.dateCreated}`);
            console.log(result);
            return res.send(`task "${req.body.name}" is still pending since ${result.dateCreated}`);
        } else {
            //actual POST
            const newTask = new Task({
                name: req.body.name,
            });

            newTask.save((savedErr, savedTask) => {
                if (err) {
                    console.log(newTask);
                    return console.error(savedErr);
                } else {
                    console.log(savedTask);
                    console.table(req.body);
                    return res.status(201).send("New task created");
                }
            });
        }
    });
});

//GET tasks
app.get("/tasks", (req, res) => {
    console.log('Someone accessed url: "' + req.url + '"');
    Task.find({}, (err, result) => {
        if (err) {
            res.send("Error in retrieving data");
        } else {
            console.log(result);
            res.send({ tasks: result });
            // return res.status(200).json({ data: data });
        }
    });
});

//SCHEMA creation - 2
const userSchema = new mongoose.Schema({
    username: String,
    password: String,
});

//MODEL
const User = mongoose.model("User", userSchema);

//Register User
app.post("/signup", (req, res) => {
    User.findOne({ username: req.body.username }, (err, result) => {
        if (err) res.send(err);
        if (
            result != null &&
            result.username == req.body.username &&
            req.body.password != "" &&
            req.body.username != ""
        ) {
            res.send(`username: ${req.body.username} already exist`);
        } else {
            const newUser = new User({
                username: req.body.username,
                password: req.body.password,
            });

            newUser.save((savedErr, savedUser) => {
                if (savedErr) {
                    return console.error(savedErr);
                } else {
                    return res.status(201).send("New user created");
                }
            });
        }
    });
});

//GET Users
app.get("/users", (req, res) => {
    User.find({}, (err, result) => {
        res.send(result);
    });
});
